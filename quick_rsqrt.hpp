#pragma once
#include <limits>
#include <memory>
#include <cmath>
#include <type_traits>

template <typename T>
concept HasQSqrtIeeeFastFloat = std::numeric_limits<float>::is_iec559 and sizeof(float) == 4;

// Standard float systems.
template <size_t Iterations = 1>
    requires (HasQSqrtIeeeFastFloat<float>)
[[gnu::always_inline, nodiscard]] constexpr float quick_rsqrt( float number ) noexcept
{
    return []<size_t ... I>( float number, std::index_sequence<I...> ) noexcept [[gnu::always_inline]]
    {
        auto x2 = number * 0.5f;
        auto y = std::bit_cast<float>(0x5f3759df - ( std::bit_cast<uint32_t>(number) >> 1 ));
        (
            (void)( y = y * ( 1.5f - ( x2 * y * y ) ), I ),  
            ...
        );
        return y;
    }( number, std::make_index_sequence<Iterations>() );
}

// Fallback for non-standard float systems.
template <size_t Iterations = 1>
    requires (not HasQSqrtIeeeFastFloat<float>)
[[gnu::always_inline, nodiscard]] constexpr float quick_rsqrt( float number ) noexcept
{
    if (std::is_constant_evaluated())
    {
        throw;
    }
    else
    {
        return 1.0f / std::sqrt(number);
    }
}